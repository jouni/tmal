use std::{
    cmp,
    io::{self, stdout, Write},
};
use tani::{Colors, Item};
use termion::{clear, cursor, style};

pub struct Tui {
    top: usize,
    selected: usize,
    items: Vec<Item>,
    colors: Colors,
}

impl Tui {
    pub fn set_items(&mut self, v: &[Item]) {
        self.top = 0;
        self.selected = 0;
        self.items = v.to_vec();
    }

    pub fn get_items(&self) -> &Vec<Item> {
        &self.items
    }

    pub fn get_mut_items(&mut self) -> &mut Vec<Item> {
        &mut self.items
    }

    pub fn new(colors: Colors) -> Self {
        Tui {
            top: 0,
            selected: 0,
            items: Vec::new(),
            colors,
        }
    }

    pub fn render(&mut self) -> io::Result<()> {
        let mut screen = stdout();
        let size = termion::terminal_size()?;
        write!(
            screen,
            "{}{}",
            cursor::Goto(size.0, size.1 - 1),
            clear::BeforeCursor
        )?;

        let mut idx = self.top;
        let w = cmp::max(0, cmp::min(100, size.0 - 29)) as usize;
        for i in 1..size.1 {
            if let Some(item) = self.items.get(idx) {
                write!(screen, "{}", cursor::Goto(1, i))?;
                if idx == self.selected {
                    write!(
                        screen,
                        "{}{}{}{:5}  {:w$}{}{}{}",
                        style::Reset,
                        self.colors.sfg.fg_string(),
                        self.colors.sbg.bg_string(),
                        idx,
                        item.line,
                        style::Reset,
                        self.colors.fg.fg_string(),
                        self.colors.bg.bg_string()
                    )?;
                } else {
                    write!(screen, "{:5}  {:w$}", idx, item.line)?;
                }
                idx += 1;
            } else {
                break;
            }
        }
        screen.flush()
    }

    pub fn select(&mut self, delta: i16) {
        let len = self.items.len();
        if len > 0 {
            self.selected = cmp::min(len - 1, cmp::max(0, self.selected as i16 + delta) as usize);
            if self.top > self.selected {
                self.top = self.selected
            } else if let Ok(size) = termion::terminal_size() {
                if self.selected > self.top + size.1 as usize - 2 {
                    self.top = cmp::min(
                        len - size.1 as usize + 1,
                        (self.top as i16 + delta) as usize,
                    );
                }
            }
        }
    }

    pub fn get_selected_idx(&self) -> usize {
        self.selected
    }

    pub fn get_selected(&self) -> Option<&Item> {
        self.items.get(self.selected)
    }

    pub fn items_len(&self) -> usize {
        self.items.len()
    }

    pub fn item_remove(&mut self, u: usize) {
        self.items.remove(u);
    }
}
