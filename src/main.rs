use configparser::ini::Ini;
use rusqlite::{params, Connection, Params};
use std::{
    cmp,
    fs::File,
    io::{self, stdin, stdout, BufRead, BufReader, Error, ErrorKind, Write},
    process::{Command, Stdio},
    sync::atomic::{AtomicBool, Ordering},
};
use tani::{Colors, Item, Keys, Line, NavigateResult, Status};
use termion::{
    clear, color, cursor, event::Key, input::TermRead, raw::IntoRawMode,
    screen::IntoAlternateScreen, style,
};

mod tui;

#[cfg(feature = "sync")]
mod sync;

macro_rules! key_loop {
    ($f:expr) => {
        || {
            let mut keys = stdin().keys();
            while let Some(Ok(k)) = keys.next() {
                let r = $f(k);
                if r.is_some() {
                    return r;
                }
            }
            None
        }
    };
}

const TABLESQL: &str = "CREATE TABLE IF NOT EXISTS ANIME(
    ID INT PRIMARY KEY NOT NULL,
    TITLE       TEXT NOT NULL,
    EPISODES    INT NOT NULL,
    STATUS      INT NOT NULL)";
const ALLSQL: &str = "SELECT * FROM ANIME ORDER BY TITLE";
const STATUSSQL: &str = "SELECT * FROM ANIME WHERE STATUS LIKE ?1 ORDER BY TITLE";

static LOCK: AtomicBool = AtomicBool::new(false);

fn bar_print(text: &str) {
    if !LOCK.load(Ordering::Relaxed) {
        if let Ok(t) = termion::terminal_size() {
            let mut screen = stdout();
            _ = write!(
                screen,
                "{}{}{}",
                cursor::Goto(1, t.1),
                clear::CurrentLine,
                text
            );
            _ = screen.flush();
        }
    }
}

fn bar_input<S>(prompt: &str, prefix: S) -> io::Result<String>
where
    S: ToString,
{
    let mut screen = stdout();
    let size = termion::terminal_size()?;

    bar_print("");
    LOCK.store(true, Ordering::Relaxed);

    let mut input = prefix.to_string();
    write!(screen, "{}{}{}", cursor::Goto(1, size.1), prompt, input)?;
    screen.flush()?;

    let res = key_loop!(|c| {
        match c {
            Key::Esc => return Some(false),
            Key::Backspace => {
                if input.pop().is_none() {
                    return Some(false);
                }
                _ = write!(screen, "{} {}", cursor::Left(1), cursor::Left(1));
            }
            Key::Char('\n') => return Some(true),
            Key::Char(x) => {
                input.push(x);
                _ = write!(screen, "{}", x);
            }
            _ => {}
        }
        _ = screen.flush();
        None
    })();

    LOCK.store(false, Ordering::Relaxed);
    bar_print("");

    if res == Some(true) {
        return Ok(input);
    }
    Err(Error::from(ErrorKind::Other))
}

fn parse_titles(xdgd: &xdg::BaseDirectories) -> Vec<Item> {
    let Some(Ok(file)) = xdgd.find_data_file("anime-titles.dat").map(File::open) else {
        return Vec::new();
    };
    BufReader::new(file)
        .lines()
        .filter_map(|line| {
            let l = line.ok()?;
            if l.starts_with(['1', '2', '3', '4', '5', '6', '7', '8', '9']) {
                let mut s = l.split('|');
                let id = s.next().map(|x| x.parse::<u32>())?.ok()?;
                if s.next() == Some("1") {
                    return Some(Item {
                        id,
                        line: Line(s.nth(1)?.to_string(), 0, 0),
                    });
                }
            }
            None
        })
        .collect()
}

fn parse_config(xdgd: &xdg::BaseDirectories) -> (Keys, Colors) {
    let mut keys = Keys::default();
    let mut colors = Colors::default();

    if let Some(cfg) = xdgd.find_config_file("tani.ini") {
        let mut config = Ini::new();
        if config.load(cfg).is_ok() {
            macro_rules! get_color {
                ($v:ident) => {
                    colors.$v = config
                        .getuint("colors", stringify!($v))
                        .and_then(|x| x.ok_or("".into()))
                        .and_then(|x| u8::try_from(x).map_err(|x| format!("{}", x)))
                        .map(|x| color::AnsiValue(x))
                        .unwrap_or(colors.$v)
                };
            }
            get_color!(sfg);
            get_color!(sbg);
            get_color!(fg);
            get_color!(bg);

            macro_rules! get_key {
                ($v:ident) => {
                    keys.$v = config
                        .get("keys", stringify!($v))
                        .and_then(|x| x.chars().next())
                        .unwrap_or(keys.$v);
                };
            }
            get_key!(up);
            get_key!(down);
            get_key!(pdown);
            get_key!(pup);
            get_key!(quit);
            get_key!(delete);
            get_key!(search);
            get_key!(edown);
            get_key!(eup);
            get_key!(status);
            get_key!(open);
            get_key!(add);
            get_key!(filter);
        }
    }
    (keys, colors)
}

fn query_items<P: Params>(
    db: &Connection,
    sql: &str,
    params: P,
) -> Result<Vec<Item>, rusqlite::Error>
where
    P: Params,
{
    db.prepare(sql)?
        .query_map(params, |r| {
            Ok(Item {
                id: r.get(0)?,
                line: Line(
                    r.get(1).unwrap_or_default(),
                    r.get(3).unwrap_or_default(),
                    r.get(2).unwrap_or_default(),
                ),
            })
        })?
        .collect()
}

fn item_status(db: &Connection, i: &Item, s: Status) -> Result<usize, rusqlite::Error> {
    db.execute(
        "UPDATE ANIME SET STATUS=(?1) WHERE ID=(?2)",
        (s as u8, i.id),
    )
}

fn item_insert(db: &Connection, i: &Item) -> Result<usize, rusqlite::Error> {
    db.execute(
        "INSERT INTO ANIME (ID,TITLE,EPISODES,STATUS) VALUES (?1,?2,?3,?4)",
        (i.id, &i.line.0, 0, 4),
    )
}

fn item_delete(db: &Connection, i: &Item) -> Result<usize, rusqlite::Error> {
    db.execute("DELETE FROM ANIME WHERE ID=(?1)", params![&i.id])
}

fn item_episode(db: &Connection, i: &Item, u: u16) -> Result<usize, rusqlite::Error> {
    db.execute("UPDATE ANIME SET EPISODES=(?2) WHERE ID=(?1)", (i.id, u))
}

fn main() -> io::Result<()> {
    let mut screen = stdout().into_raw_mode()?.into_alternate_screen()?;

    write!(screen, "{}", termion::cursor::Hide)?;

    let xdgd =
        xdg::BaseDirectories::with_prefix("tani").expect("failed to get xdg base directories");
    let dbf = xdgd
        .place_data_file("anime.db")
        .expect("failed to create data path");
    let db = Connection::open(dbf).expect("failed to open db");
    db.execute(TABLESQL, ()).expect("failed to create table");

    #[cfg(feature = "sync")]
    let handle = {
        let tf = xdgd.place_data_file("time")?;
        let out = xdgd.place_data_file("anime-titles.dat")?;
        sync::sync(tf, out)
    };

    let (k, colors) = parse_config(&xdgd);
    write!(
        screen,
        "{}{}{}",
        style::Reset,
        colors.fg.fg_string(),
        colors.bg.bg_string()
    )?;
    bar_print("");

    let mut tui = tui::Tui::new(colors);

    _ = query_items(&db, ALLSQL, params![]).inspect(|x| tui.set_items(x));

    let navigate = |tui: &mut tui::Tui, c| {
        if let Ok(s) = termion::terminal_size() {
            match c {
                '\n' => return NavigateResult::Select,
                x if x == k.quit => return NavigateResult::Quit,
                x if x == k.up => tui.select(-1),
                x if x == k.down => tui.select(1),
                x if x == k.pup => tui.select(-(s.1 as i16 - 1)),
                x if x == k.pdown => tui.select(s.1 as i16 - 1),
                x if ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].contains(&x) => {
                    if let Ok(x) = bar_input("Goto: ", c) {
                        if let Ok(x) = x.parse::<i16>() {
                            tui.select(cmp::max(0, x) - tui.get_selected_idx() as i16);
                        }
                    }
                }
                _ => {}
            }
        }
        NavigateResult::Continue
    };

    let update_episode = |tui: &mut tui::Tui, d: i16| {
        let idx = tui.get_selected_idx();
        let itm = tui.get_mut_items();
        if !itm.is_empty() {
            if let Some(i) = itm.get_mut(idx) {
                let e = (i.line.2 as i32 + d as i32).clamp(0, u16::MAX.into()) as u16;
                if item_episode(&db, i, e).is_ok() {
                    i.set_episode(e);
                }
            }
        }
    };

    let title_like = |v: Vec<Item>, t: &str| -> Vec<Item> {
        v.into_iter()
            .filter(|x| x.line.0.to_lowercase().contains(&t.to_lowercase()))
            .collect()
    };

    tui.render()?;
    key_loop!(|kk| {
        if let Key::Char(c) = kk {
            if navigate(&mut tui, c) == NavigateResult::Quit {
                return Some(());
            }
            match c {
                x if x == k.open => {
                    tui.get_selected().inspect(|x| {
                        _ = Command::new("xdg-open")
                            .stdout(Stdio::null())
                            .stderr(Stdio::null())
                            .arg(format!("https://anidb.net/anime/{}", x.id))
                            .spawn();
                    });
                }
                x if x == k.search => {
                    if let Ok(t) = bar_input("Search: ", "") {
                        if let Ok(i) = query_items(&db, ALLSQL, params![]) {
                            tui.set_items(&title_like(i, &t));
                        }
                    }
                }
                x if x == k.filter => {
                    bar_print("Filter status: (w)atching (c)ompleted (d)ropped (p)lan to watch.");
                    key_loop!(|kk| {
                        if let Key::Char(c) = kk {
                            _ = (if let Ok(s) = Status::try_from(c) {
                                query_items(&db, STATUSSQL, params![s as u8])
                            } else {
                                query_items(&db, ALLSQL, params![])
                            })
                            .inspect(|x| tui.set_items(x));
                        }
                        Some(())
                    })();
                    bar_print("");
                }
                x if x == k.add => {
                    if let Ok(t) = bar_input("Add: ", "") {
                        tui.set_items(&title_like(parse_titles(&xdgd), &t));

                        _ = tui.render();
                        key_loop!(|kk| {
                            if let Key::Char(c) = kk {
                                match navigate(&mut tui, c) {
                                    NavigateResult::Select => {
                                        tui.get_selected().inspect(|x| _ = item_insert(&db, x));
                                        return Some(());
                                    }
                                    NavigateResult::Quit => return Some(()),
                                    _ => _ = tui.render(),
                                }
                            }
                            None
                        })();

                        _ = query_items(&db, ALLSQL, params![]).inspect(|x| tui.set_items(x));
                    }
                }
                x if x == k.delete => {
                    let itm = tui.get_items();
                    if !itm.is_empty() {
                        let idx = tui.get_selected_idx();
                        itm.get(idx).inspect(|x| _ = item_delete(&db, x));
                        tui.item_remove(idx);
                        if idx == tui.items_len() {
                            tui.select(-1);
                        }
                    }
                }
                x if x == k.status => {
                    bar_print("Set status: (w)atching (c)ompleted (d)ropped (p)lan to watch.");
                    key_loop!(|kk| {
                        if let Key::Char(c) = kk {
                            if let Ok(s) = Status::try_from(c) {
                                let idx = tui.get_selected_idx();
                                let itm = tui.get_mut_items();
                                if !itm.is_empty() {
                                    if let Some(i) = itm.get_mut(idx) {
                                        if item_status(&db, i, s).is_ok() {
                                            i.set_status(s);
                                        }
                                    }
                                }
                            }
                        }
                        Some(())
                    })();
                    bar_print("");
                }
                x if x == k.eup => update_episode(&mut tui, 1),
                x if x == k.edown => update_episode(&mut tui, -1),
                _ => {}
            }
            _ = tui.render();
        }
        None
    })();

    #[cfg(feature = "sync")]
    if !handle.is_finished() {
        bar_print("Waiting for sync to finish...");
        _ = handle.join();
    }

    write!(screen, "{}{}", termion::style::Reset, termion::cursor::Show)
}
